# What is kubernetes?

## Open source container orchestrator tools
    Developed by Google
        Helps you manage your containerzed applications
        in different environments

## What problems kubernetes solves?
    Increase usage of containers  
    Microservices

## What features do orchestration tools offer?

    High availability or no downtime
    Scalibility or high performance 
    Disasterr recovery - backup and restore

---

## K8s components
    - Node (server or virtual machine)
        - Application -> pod 
            - Container -> Docker 
    Every pod gets virtual IP 
        When container dies because of resources or a app crash it gets new IP
        - Service(communication between pods):
            Permanent IP
            Lifecycle of POD and Service NOT Connected 
    External service to open app to internet
    Internal service for databases 
        Ingress(https and domain name) forward to Service
    
    - ConfigMap:
        external configuration of your applications
            e.g DB_URL = mongo-db 
        Secret (for secrete data(credentials(base64 encoded)))
    
    - Volumes(data storage) 
        - attached physical storage on HDD to a pod (can be remote(cloud storage))
        If pod restarts data is safe 
    
    Replication 
    Node 1 dies, Node 2 have a replica  
        blueprint(deployment) for pods (how many replicas you want)
        abstarction of Pods 
        You can't replicate DBs with deployments because has state ITS DATA. 
         Mechanism for dbs 0 StatefulSet - making sure dbs read and write with consistency

--

## Kubernetes Architecture
    - Node procceses
        - each Node has multiple Pods on internet
        - 3 procceses must be installed on every Node 
            - container runtime(Docker)
            - kubelet interacts with both container and node 
            - kube proxy forwards the request

--

## How do you interact with this cluster?
    Schedule pods 
    monitor
    re-schedule/restart pods
    join new Node

    Are managing procceses are managed by Master node
    - 4 procceses must run on Master
        - API server (cluster gateway) - acts as a gatekeeper for authentication
            API > validates request > other procceses > Pods 
        - Scheduler just decides on which Node new pod should be scheduled 
        - Controller manager detect state changes (crashed pods)
            Controller manager > Scheduler > Kubelet (restart the pods)
        - etcd key value (cluster brain) saved into key value > etcd)
            All node state is saved in etcd (cluster information) 

--

## What is minikube?
    - One node cluster for Test/Local testing (VirtualBox)
## What is kubectl?
    - Interact with cluster, pods etc.
    - Command line for K8s - submits API request to edit, enable, create etc on Nodes 
## What is namespaces?
    - Namespaces is cluster inside a cluster 
    - Default namespaces:
        - kube-system
        - kube-public
        - kube-node-lease
        - default 
    Usa cases:
        - Strcutre your components 
        - Avoid conflicts between teams 
        - Share services between different environments 
        - Access and resource limits on Namespaces level

--

## What is helm?
    - Package Manager for Kubernetes 
        - To package YAML files and distribute them in public and private repositories
## Helm charts
    - Bundle of YAML files 
    - Create your own Helm charts with helm
    - Push them to helm repositories
    - Downlaod and use existing ones 

## What is stateful application?
    - Stateful apps are databases(application that store data in some storage and keep track and record)

    