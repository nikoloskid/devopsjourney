# Definitions 

What is DevOps?
- DevOps is the union of people, process, and products to enable continuous delivery of value to our customers. However, what exactly does that mean? Let's join the team as Mara explains what DevOps is, what it isn't, and what makes elite performers successful.

## Consists
Agile planning. Together, we'll create a backlog of work that everyone on the team and in management can see. We'll prioritize the items so we know what we need to work on first. The backlog can include user stories, bugs, and any other information that helps us.

Scrum. Scrum depends on a Scrum master who leads the Scrum team. The Scrum master makes sure everybody understands Scrum theory, practices, and rules. We don't have a Scrum master; that's someone who's usually received some training and certification so I didn't pick that one either.

Continuous integration (CI). We'll automate how we build and test our code. We'll run that every time a team member commits changes to version control.

Continuous delivery (CD). CD is how we test, configure, and deploy from a build to a QA or production environment.

Monitoring. We'll use telemetry to get information about an application's performance and usage patterns. We can use that information to improve as we iterate.

Infrastructure as Code (IaC): Enables the automation and validation of the creation and teardown of environments to help deliver secure and stable application hosting platforms.

Use Microservices architecture to isolate business use cases into small reusable services that communicate via interface contracts. This architecture enables scalability and efficiency.

Containers are the next evolution in virtualization. They're much more lightweight than virtual machines, allow much faster hydration, and easily configure files.

# What is Azure Pipelines?

Microsoft Azure Pipelines is a cloud service you can use to automatically build, test, and deploy your code project. You can also make it available to other users, and it works with just about any language or project type.
    * [More read here](https://docs.microsoft.com/en-us/training/modules/create-a-build-pipeline/2-what-is-azure-pipelines)